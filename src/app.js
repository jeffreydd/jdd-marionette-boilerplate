var Marionette = require('backbone.marionette');

require('bootstrap');
require('../node_modules/bootstrap/dist/css/bootstrap.min.css');

module.exports = Marionette.Application.extend({
  initialize: function(options) {
    console.log('Initialising App with options:', options);
  }
});
